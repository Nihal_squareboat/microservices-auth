<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($this->isApiRoute($request))
            return $this->renderJSON($request, $exception);

        return parent::render($request, $exception);
    }

    /**
     * Render an exception into an JSON response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function renderJSON($request, Exception $exception)
    {
        switch ($exception)
        {
            case ($exception instanceof ExpiredCodeException):
                return response()->error("Invalid code", 403, "Invalid code");
            case ($exception instanceof IncorrectCredentialsException):
                return response()->error("Incorrect Username or Password", 422, "Incorrect Username or Password");
            case ($exception instanceof UserNotConfirmedException):
                return response()->error("User is not confirmed", 422, "User is not confirmed");
            case ($exception instanceof UsernameExistsException):
                return response()->error("User already exists", 409, "User already exists");
            case ($exception instanceof OtherException):
                return response()->error( $exception->getMessage(), 417, $exception->getMessage() );
            case ($exception instanceof CredentialsNotFoundException):
                return response()->error( $exception->getMessage(), 422, $exception->getMessage() );
            case ($exception instanceof ValidationException):
                return response()->error($exception->errors()->toArray(), 422);
            case ($exception instanceof UserNotFoundException):
                return response()->error("User does not exist", 410, "User does not exist.");
            case ($exception instanceof InvalidPasswordException):
                return response()->error("Password should have a uppercase alphabet, lowercase alphabet, a number and a special symbol.", 422, "Password should have a uppercase alphabet, lowercase alphabet, a number and a special symbol.");
            default : 
                return parent::render($request, $exception);
        }
    }

    protected function isApiRoute($request) {
        return $request->segment(1) == 'api';
    }
}
