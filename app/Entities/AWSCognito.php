<?php

namespace App\Entities;

use App\Exceptions\OtherException;
use App\Exceptions\ExpiredCodeException;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\UsernameExistsException;
use App\Exceptions\InvalidPasswordException;
use App\Exceptions\UserNotConfirmedException;
use App\Exceptions\IncorrectCredentialsException;
use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;

class AWSCognito
{

    private $region;
    private $client_id;
    private $userpool_id;
    private $client;

    /**
     * Constructor
    */
    public function __construct()
    {
        $this->region = config('aws.region');
        $this->client_id = config('aws.client_id');
        $this->userpool_id = config('aws.userpool_id');
        $this->client = $this->getClient();
    }

    private function getClient()
    {
        return new CognitoIdentityProviderClient([ 'version' => '2016-04-18', 'region' => $this->region ]);
    }

    public function authenticate($username, $password)
    {
        try {
            $result = $this->client->adminInitiateAuth([
                'AuthFlow' => 'ADMIN_NO_SRP_AUTH',
                'ClientId' => $this->client_id,
                'UserPoolId' => $this->userpool_id,
                'AuthParameters' => [
                    'USERNAME' => $username,
                    'PASSWORD' => $password,
                ],
            ]);
        } catch (\Exception $e) {
            if (stripos($e->getMessage(), "UserNotConfirmedException") !== false)
                throw new UserNotConfirmedException();
            if (stripos($e->getMessage(), "NotAuthorizedException") !== false)
                throw new IncorrectCredentialsException();
            throw new OtherException($e->getMessage());
        }
        return $result->get('AuthenticationResult');
    }
    
    public function signup($email, $password, $name, $phone)
    {
        try {
            $result = $this->client->signUp([
                'ClientId' => $this->client_id,
                'Username' => $email,
                'Password' => $password,
                'UserAttributes' => [
                    [
                        'Name' => 'name',
                        'Value' => $name
                    ],
                    [
                        'Name' => 'email',
                        'Value' => $email
                    ],
                    [
                        'Name' => 'phone_number',
                        'Value' => $phone
                    ]
                ],
            ]);
        } catch (\Exception $e) {

            if (stripos($e->getMessage(), "UsernameExistsException") !== false) {
                throw new UsernameExistsException();
            }
            if (stripos($e->getMessage(), "InvalidPasswordException") !== false) {
                throw new InvalidPasswordException();
            }
            throw new OtherException($e->getMessage());
        }
        return $result;
    }

    public function sendPasswordResetMail($email)
    {
        try {
            $this->client->forgotPassword([
                'ClientId' => $this->client_id,
                'Username' => $email
            ]);
        } catch (\Exception $e) {
            throw new OtherException($e->getMessage());
        }
        return true;
    }

    public function resetPassword($email, $password, $code)
    {
        try {
            $result = $this->client->confirmForgotPassword([
                'ClientId' => $this->client_id,
                'ConfirmationCode' => $code,
                'Password' => $password,
                'Username' => $email
            ]);
        } catch (\Exception $e) {
            if (stripos($e->getMessage(), "ExpiredCodeException") !== false) {
                throw new ExpiredCodeException();
            }
            throw new OtherException($e->getMessage());
        }
        return true;
    }

    public function getPoolMetadata()
    {
        $result = $this->client->describeUserPool([
            'UserPoolId' => $this->userpool_id,
        ]);

        return $result->get('UserPool');
    }

    public function getuserDetails($token)
    {
        try {
            $result = $this->user = $this->client->getUser([
                'AccessToken' => $token
            ]);
        } catch(\Exception  $e) {
            if (stripos($e->getMessage(), "UserNotFoundException") !== false) {
                throw new UserNotFoundException();
            }
            
            throw new OtherException($e->getMessage());
        }
        return $result->get('UserAttributes');
    }

    public function getPoolUsers()
    {
        $result = $this->client->listUsers([
            'UserPoolId' => $this->userpool_id,
        ]);
        
        return $result->get('Users');
    }

    
    
}