<?php

namespace App\Validators;

use App\Rules\PhoneNumber;

class AWSValidator extends Validator
{
    /**
    * Validation rules.
    *
    * @param  string $type
    * @param  array $data
    * @return array
    */

    protected function rules($data, $type)
    {
        $rules = [];

        switch($type)
        {
            case 'authenticate':
                $rules = [
                    'username'    => 'required',
                    'password' => 'required|min:8'
                ];
                break;
            case 'signup':
                $rules = [
                    'email' => 'required|email',
                    'password' => 'required|min:8',
                    'name' => 'required',
                    'phone' => ['required', new PhoneNumber]
                ];
                break;
            case 'sendPasswordResetMail':
                $rules = [
                    'email'    => 'required'
                ];
                break;
            case 'resetPassword':
                $rules = [
                    'code'    => 'required|integer',
                    'password' => 'required|min:8',
                    'email' => 'required|email'
                ];
                break;
            case 'userDetails':
                $rules = [
                    'token'    => 'required'
                ];
                break;
        }

        return $rules;
    }
}
