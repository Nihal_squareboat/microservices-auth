<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
  /**
   * Perform post-registration booting of services.
   *
   * @return void
   */
  public function boot()
  {
    // success macro
    Response::macro('success', function ($data = null, $status = 200, $message= null) {     

      return response()->json([
        'success'  => true,
        'status' => $status,
        'message' => $message,
        'data' => is_array($data) ?  array_except($data, 'meta') : $data,
        'meta' => is_array($data) ?  array_get($data, 'meta') : null
      ], $status);
    });

    Response::macro('error', function ($errors, $status = 400, $message= null) {
      $errors = is_array($errors) ? $errors : ['message' => $errors];

      return response()->json([
        'success'  => false,
        'status' => $status,
        'message' => $message,
        'errors' => $errors
      ], $status);
    });
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}