<?php

namespace App\Services;

use App\Entities\AWSCognito;
use App\Validators\AWSValidator;

class AWSCognitoService
{
    protected $cognito;
    public $validator;

    public function __construct(AWSCognito $cognito, AWSValidator $validator)
    {
        $this->cognito = $cognito;
        $this->validator = $validator;
    }

    /**
     * Handle login.
     *
     * @param  array         $inputs
     * 
     */
    public function authenticate($inputs) {
      $this->validator->fire($inputs, 'authenticate');
		return $this->cognito->authenticate($inputs['username'], $inputs['password']);
    }

    /**
     * Handle signup.
     *
     * @param  array         $inputs
     * 
     */
    public function signup($inputs) {
      $this->validator->fire($inputs, 'signup');
		  return $this->cognito->signup($inputs['email'], $inputs['password'], $inputs['name'], $inputs['phone']);
    }
  
    /**
     * Send reset password code.
     *
     * @param  array         $inputs
     * 
     */
    public function sendPasswordResetMail($inputs) {
      $this->validator->fire($inputs, 'sendPasswordResetMail');
		  return $this->cognito->sendPasswordResetMail($inputs['email']);
    }

    /**
     * Reset password using code.
     *
     * @param  array         $inputs
     * 
     */
    public function resetPassword($inputs) {
      $this->validator->fire($inputs, 'resetPassword');
		  return $this->cognito->resetPassword($inputs['email'], $inputs['password'], $inputs['code']);
    }

    /**
     * 
     * Get pool metadata
     * 
     */
    public function getPoolMetadata() {
		  return $this->cognito->getPoolMetadata();
    }

    /**
     * 
     * Get user details
     * 
     */
    public function getuserDetails($inputs) {
      $this->validator->fire($inputs, 'userDetails');
		  return $this->cognito->getuserDetails($inputs['token']);
    }

    /**
     * 
     * Get all users
     * 
     */
    public function getPoolUsers() {
		  return $this->cognito->getPoolUsers();
    }

}