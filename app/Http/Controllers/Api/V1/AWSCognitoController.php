<?php
namespace App\Http\Controllers\Api\V1;

use App\Services\AWSCognitoService;
use App\Exceptions\CredentialsNotFoundException;

class AWSCognitoController
{
    private $service;

    private $user = null;

    public function __construct(AWSCognitoService $cognito_service)
    {
        $this->service = $cognito_service;
    }

    public function authenticate()
    {
        $inputs = request()->all();
        $response = app()->call([$this->service, 'authenticate'], ['inputs' => $inputs]);
        return response()->success($response);
    }

    public function signup()
    {
        $inputs = request()->all();
        $response = app()->call([$this->service, 'signup'], ['inputs' => $inputs]);
        if($response)
            return response()->success(null, 201, 'User created successfully.');
        return response()->error("Something went wrong!");
    }

    public function sendPasswordResetMail()
    {
        $inputs = request()->all();
        $response = app()->call([$this->service, 'sendPasswordResetMail'], ['inputs' => $inputs]);
        if($response)
            return response()->success(null, 200, 'Reset password mail send!');
        return response()->error("Something went wrong!");
    }

    public function resetPassword()
    {
        $inputs = request()->all();
        $response = app()->call([$this->service, 'resetPassword'], ['inputs' => $inputs]);
        if($response)
            return response()->success(null, 200, 'Password changed successfully');
        return response()->error("Something went wrong!");
    }

    public function getPoolMetadata()
    {
        $response = app()->call([$this->service, 'getPoolMetadata']);
        return response()->success($response);
    }

    public function getuserDetails()
    {
        $inputs = request()->all();
        $response = app()->call([$this->service, 'getuserDetails'], ['inputs' => $inputs]);
        return response()->success($response);
    }

    public function getPoolUsers()
    {
        $response = app()->call([$this->service, 'getPoolUsers']);
        return response()->success($response);
    }

}
