<?php

return [
    'region' => env('REGION'),
    'client_id' => env('CLIENT_ID'),
    'userpool_id' => env('USERPOOL_ID')
];
