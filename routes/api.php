<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AWSCognitoController@authenticate')->name('login');
Route::post('/signup', 'AWSCognitoController@signup')->name('signup');
Route::post('/forgot-password', 'AWSCognitoController@sendPasswordResetMail')->name('forgot-password');
Route::post('/reset-password', 'AWSCognitoController@resetPassword')->name('reset-password');
Route::get('/pool-metadata', 'AWSCognitoController@getPoolMetadata')->name('pool-metadata');
Route::post('/user-details', 'AWSCognitoController@getuserDetails')->name('user-details');
Route::get('/pool-users', 'AWSCognitoController@getPoolUsers')->name('pool-users');

Route::get('/', function () {
    return view('welcome');
});